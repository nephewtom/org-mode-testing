#+TITLE: Org mode TOC in Gitlab

* Table of Contents                                                     :TOC:
- [[#introduction][Introduction]]
- [[#body][Body]]
  - [[#first-part][First part]]
  - [[#second-part][Second part]]
  - [[#third-part][Third part]]
- [[#conclusion][Conclusion]]

* Introduction
This is the nice and cool introduction.

* Body
This great body is divided in three parts.

** First part
Talks about the first part and contains two topics.
*** Topic A
Interesting topic A.
*** Topic B
Awesome topic B.

** Second part
This is the amazing second part.

** Third part
Tells about the interesting third part.

* Conclusion
Finally, this is the great conclusion.
